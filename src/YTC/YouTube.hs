module YTC.YouTube where

import           Control.Arrow              ((>>>))
import           Control.Lens
import           Control.Monad.Except       (ExceptT, liftEither, runExceptT)
import           Control.Monad.Trans        (lift, liftIO)
import           Data.Aeson                 (eitherDecode')
import           Data.Aeson.Lens
import           Data.Aeson.TH              hiding (Options)
import           Data.Aeson.Types           hiding (Options, Result)
import           Data.Bool                  (bool)
import qualified Data.ByteString            as BS
import qualified Data.ByteString.Char8      as C
import qualified Data.ByteString.Lazy       as BL
import qualified Data.ByteString.Lazy.Char8 as CL
import           Data.Either.Combinators    (mapLeft, mapRight, maybeToRight)
import           Data.Function              ((&))
import           Data.List                  (intercalate)
import           Data.Maybe                 (fromMaybe, listToMaybe)
import qualified Data.Text                  as T
import           Data.Text.ICU.Regex        (findNext, regex', setText)
import qualified Data.Text.Lazy             as TL
import           Data.Time.ISO8601          (parseISO8601)
import           Network.Wreq               (get, responseBody, responseStatus, statusCode)
import           Prelude                    hiding (id, reverse)
import qualified Prelude                    as Pre

import           YTC.Data
import           YTC.Utils

parseResponseBody :: String -> ExceptT String IO YTResponse
parseResponseBody x = x & stringToCL & eitherDecode' & mapLeft (<> "\n" <> x) & liftEither

ytItemToVideo :: String -> YTItem -> ExceptT String IO Video
ytItemToVideo label x = do
  date <- s ^. publishedAt & parseISO8601 & maybeToRight "" & liftEither
  return
    Video
      { _videoId = x ^. id . videoId
      , _videoDate = date
      , _videoTitle = s ^. title & decodeHtmlEntities
      , _videoThumbnail = s ^. thumbnails . high . url
      , _videoChannelTitle = label
      , _videoAction = Nothing
      }
  where
    s = x ^. snippet

ytResponseToResult :: String -> YTResponse -> ExceptT String IO Result
ytResponseToResult title x = do
  videos <- x ^. items & mapM (ytItemToVideo title)
  return Result {_resultVideos = videos}

operationApplies :: Video -> Operation -> ExceptT String IO Bool
operationApplies vid op = do
  rgxE <- liftIO $ regex' [] (op ^. regex & T.pack) <&> mapLeft show
  rgx <- liftEither $ rgxE
  liftIO $ setText rgx (T.pack $ vid ^. title)
  found <- liftIO $ findNext rgx
  liftEither $ Right found

fillAction :: Config -> Video -> ExceptT String IO Video
fillAction cfg v = do
  let allOps = cfg ^. operations
  appliedOps <- mapM (operationApplies v) allOps
  let actions = appliedOps & zip allOps & filter snd <&> fst <&> (^. action)
  liftEither $ Right $ v & action .~ listToMaybe actions

fillActions :: Config -> Result -> ExceptT String IO Result
fillActions cfg res = do
  vids <- mapM (fillAction cfg) (res ^. videos)
  liftEither $ Right $ res & videos .~ vids

getVideos :: Config -> Channel -> ExceptT String IO Result
getVideos cfg channel = do
  let key = cfg ^. apiKey
  let urlPrefix = "https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&channelId="
  let channelId = channel ^. id
  let limit = cfg ^. maxResults
  let url = urlPrefix <> channelId <> "&maxResults=" <> show limit <> "&order=date&key=" <> key
  r <- lift $ get url
  let stCode = r ^. responseStatus . statusCode
  if stCode == 200
    then processSuccess $ r ^. responseBody & CL.unpack
    else liftEither $ Left $ "Server reponded with error code " <> show stCode <> ": " <> CL.unpack (r ^. responseBody)
  where
    processSuccess :: String -> ExceptT String IO Result
    processSuccess body = do
      parsedResp <- parseResponseBody body
      ytResponseToResult (channel ^. label) parsedResp

getData :: Config -> ExceptT String IO Result
getData cfg = do
  results <- mapM (getVideos cfg) (cfg ^. channels)
  let resultWithoutActions = results & mconcat
  let resultWithActions = fillActions cfg resultWithoutActions
  resultWithActions <&> handleReverse
  where
    handleReverse = videos %~ applyReverse
    applyReverse = bool Pre.id Pre.reverse (cfg ^. reverse & fromMaybe False)
