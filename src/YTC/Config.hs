module YTC.Config where

import           Control.Arrow        ((>>>))
import           Control.Monad.Except (ExceptT, liftEither, liftIO)
import           Data.Aeson           (eitherDecode)

import           YTC.Data             (Config)
import           YTC.Utils

parseConfig :: String -> ExceptT String IO Config
parseConfig = stringToCL >>> eitherDecode >>> liftEither

readConfig :: String -> ExceptT String IO Config
readConfig fName = do
  str <- liftIO $ readFile fName
  parseConfig str
