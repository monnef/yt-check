{-# LANGUAGE OverloadedStrings #-}

module YTC.UtilsTH where

import           Control.Lens                  ((&))
import           Control.Lens.Internal.FieldTH (_fieldToDef, makeFieldOptics)
import           Control.Lens.TH
import           Data.Char                     (toLower)
import           Language.Haskell.TH           (DecsQ, Name)

convertFieldName :: String -> String -> String
convertFieldName prefix x = x & drop (length prefix) & decapitalizeWord

decapitalizeWord :: String -> String
decapitalizeWord "" = ""
decapitalizeWord x  = (x & head & toLower & (: [])) <> (x & tail)

myMakeFields :: Name -> DecsQ
myMakeFields = makeFieldOptics myRules
  where
    myRules = defaultFieldRules {_fieldToDef = mapper}
    mapper tName fNames fName = undefined -- TODO

