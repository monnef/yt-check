module Main where

import           Control.Lens
import           Control.Monad.Except (ExceptT, runExceptT)
import           Control.Monad.Trans  (lift, liftIO)
import           Data.Bifoldable      (bimapM_)
import           Data.Function        ((&))
import           Data.Maybe           (fromMaybe)
import           Prelude              hiding (id, reverse)
import           System.Console.ANSI
import           System.Exit          (ExitCode (..), exitWith)
import           System.IO            (hPutStrLn, stderr)

import           Control.Monad        (void)
import           YTC.Data
import           YTC.Lib
import           YTC.TUIList

cfgFileName :: String
cfgFileName = "yt-check.json"

obtainConfig :: ExceptT String IO Config
obtainConfig = cfgFileName & readConfig

obtainData :: Config -> ExceptT String IO Result
obtainData = getData

basicPrintData :: Config -> Result -> ExceptT String IO ()
basicPrintData _ res = mapM_ printVid $ res ^. videos
  where
    printVid :: Video -> ExceptT String IO ()
    printVid v = do
      let act = fromMaybe NoAction (v ^. action)
      let fv = v & formatVideo
      case act of
        Hide -> return ()
        _ -> do
          lift $ setSGR [SetColor Foreground (colorIntensity act) (color act)]
          lift $ setSGR [SetConsoleIntensity (consoleIntensity act)]
          fv & putStrLn & lift
          lift $setSGR [Reset]
          lift $ setSGR [SetColor Foreground Vivid White]
          where colorIntensity NoAction  = Vivid
                colorIntensity Highlight = Vivid
                colorIntensity _         = Dull
                color Highlight = Green
                color SemiHide  = Cyan
                color _         = White
                consoleIntensity SemiHide  = FaintIntensity
                consoleIntensity Highlight = BoldIntensity
                consoleIntensity _         = NormalIntensity

runListTUI :: Config -> Result -> ExceptT String IO ()
runListTUI cfg res = res & listTUI cfg & liftIO & runExceptT & void

presentData :: Config -> Result -> ExceptT String IO ()
presentData cfg res = do
  let presenter =
        case cfg ^. interface of
          RawTerminal -> \_ r -> lift $ mapM_ print (r ^. videos)
          Terminal    -> basicPrintData
          TUIList     -> runListTUI
  presenter cfg res

app :: ExceptT String IO ()
app = do
  cfg <- obtainConfig
  res <- obtainData cfg
  presentData cfg res

crash :: String -> IO ()
crash err = do
  hPutStrLn stderr err
  exitWith $ ExitFailure 1

main :: IO ()
main = do
  res <- app & runExceptT
  bimapM_ crash pure res
