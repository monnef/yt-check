{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE LambdaCase             #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE PartialTypeSignatures  #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}

module YTC.TUIList
  ( listTUI
  ) where

import           Control.Arrow              ((>>>))
import           Control.Lens               (element, makeFields, to, (<&>), (^.), (^?!))
import           Control.Monad              (forM_, void, when)
import           Control.Monad.IO.Class     (liftIO)
import           Data.Function              (on, (&))
import           Data.Maybe                 (fromMaybe, listToMaybe)
import qualified Data.Text                  as Txt
import qualified Data.Text.Lazy             as TxtL
import qualified Data.Text.Template         as Tmpl
import qualified Data.Vector                as Vec
import           GHC.Generics               (Generic)
import qualified Graphics.Vty               as V
import           Prelude                    hiding (id)
import qualified Prelude                    as Pre
import           System.Process             (runCommand, waitForProcess)

import qualified Brick.AttrMap              as A
import qualified Brick.BChan                as BBch
import qualified Brick.Main                 as M
import qualified Brick.Types                as T
import qualified Brick.Util                 as BU
import qualified Brick.Widgets.Border       as B
import           Brick.Widgets.Border.Style (unicode)
import           Brick.Widgets.Center       (hCenter)
import qualified Brick.Widgets.Center       as C
import           Brick.Widgets.Core         (fill, hLimit, hLimitPercent, joinBorders, padLeft, padLeftRight, padRight,
                                             padTop, padTopBottom, reportExtent, str, strWrap, vLimit, withAttr,
                                             withBorderStyle, (<+>), (<=>))
import           Brick.Widgets.List         (listElementsL)
import qualified Brick.Widgets.List         as L

import           YTC.Data
import           YTC.Lib
import           YTC.Video

data WName
  = VideoList
  | ImagePreview
  deriving (Show, Generic, Eq, Ord)

data CEvent =
  RefreshThumbnail
  deriving (Show, Generic, Eq, Ord)

instance Show (BBch.BChan a) where
  show _ = "BChan"

data St =
  St
    { _stResult       :: Result
    , _stVideoList    :: L.List WName Video
    , _stConfig       :: Config
    , _stEventChannel :: BBch.BChan CEvent
    }
  deriving (Show, Generic)

makeFields ''St

getSelectedVideoIndex :: St -> Maybe Int
getSelectedVideoIndex st = st ^. videoList . L.listSelectedL

getSelectedVideo :: St -> Maybe Video
getSelectedVideo st = getSelectedVideoIndex st <&> \idx -> st ^. videoList . listElementsL ^?! element idx

-- From videoList only selected item is compared
instance Eq St where
  (==) = (==) `on` ex
    where
      ex x = (x ^. config, x ^. result, getSelectedVideoIndex x)

imageWidget :: T.Widget WName
imageWidget =
  T.Widget T.Greedy T.Greedy $ do
    ctx <- T.getContext
    let maxHeight = ctx ^. T.availHeightL
    let maxWidth = ctx ^. T.availWidthL
    T.render $ vLimit maxHeight (hLimit maxWidth $ fill ' ')

thumbPathFromSt :: St -> Maybe String
thumbPathFromSt st = st & getSelectedVideo <&> (^. thumbnail)

drawUI :: St -> [T.Widget WName]
drawUI st = [ui]
  where
    list = st ^. videoList
    curIdxMay = getSelectedVideoIndex st
    cur = getSelectedVideo st
    listW = L.renderList listDrawElement True list
    listHeader =
      hCenter (str $ fromMaybe "?" (curIdxMay <&> ((+ 1) >>> show)) <> " / " <> show (list ^. L.listElementsL & length))
    listContainer = hLimitPercent 25 $ listHeader <=> listW
    detailStr = cur <&> formatVideo & fromMaybe "No video selected." & strWrap
    thumb = reportExtent ImagePreview imageWidget
    detailBox = padLeftRight 3 $ padTopBottom 1 $ thumb <=> padTop (T.Pad 2) (C.hCenter detailStr)
    ui =
      withBorderStyle unicode $ joinBorders $ B.borderWithLabel (str "YouTube Check by monnef") $ --
      listContainer <+>
      B.vBorder <+>
      detailBox

context :: [(String, String)] -> Tmpl.Context
context arr x = lookup (Txt.unpack x) arr <&> Txt.pack & fromMaybe ("?" <> x <> "?")

drawThumbnailFromExt :: St -> T.Extent WName -> T.EventM WName ()
drawThumbnailFromExt state (T.Extent ImagePreview upperLeft (w, h) _) = do
  let (x, y) = upperLeft ^. T.locL
  let pathMay = thumbPathFromSt state
  let cfg :: Config = state ^. config
  let tmplCtx path = context [("path", path), ("x", show x), ("y", show y), ("width", show w), ("height", show h)]
  let pathToCmd path = Tmpl.substitute (Txt.pack $ fromMaybe "" $ cfg ^. previewCommand) (tmplCtx path) & TxtL.unpack
  let cmdStr = maybe "" pathToCmd pathMay
  procH <- liftIO $ runCommand cmdStr
  void $ liftIO $ waitForProcess procH
drawThumbnailFromExt _ _ = error "invalid extent"

controlActionFor :: St -> V.Event -> Maybe ControlAction
controlActionFor st ev =
  st ^. config . controls & fromMaybe [] & filter (^. event . to (== ev)) & listToMaybe <&> (^. action)

processControls :: St -> V.Event -> T.EventM n (T.Next St)
processControls st (V.EvKey V.KEsc []) = M.halt st
processControls st (V.EvKey (V.KChar 'q') []) = M.halt st
processControls st ev =
  case controlActionFor st ev of
    Just QuitControlAction -> M.halt st
    Just (RunCommandControlAction cmd) ->
      case getSelectedVideo st of
        Nothing -> M.continue st
        Just vid -> do
          let tmplCtx = context [("url", fullYTVideoUrl vid), ("id", vid ^. id)]
          let cmdStr = Tmpl.substitute (Txt.pack cmd) tmplCtx & TxtL.unpack
          let run = do
                procH <- runCommand cmdStr
                void $ waitForProcess procH
                BBch.writeBChan (st ^. eventChannel) RefreshThumbnail
                return st
          M.suspendAndResume run
    Nothing -> M.continue st

drawThumbnail :: St -> T.EventM WName ()
drawThumbnail st = do
  mExtent <- M.lookupExtent ImagePreview
  forM_ mExtent (drawThumbnailFromExt st)

--skipEvent =
--  \case
--    T.VtyEvent (V.EvResize _ _) -> ()
--    T.VtyEvent V.EvGainedFocus -> ()
--    T.AppEvent RefreshThumbnail -> ()
--
--appEvent :: St -> T.BrickEvent WName CEvent -> T.EventM WName (T.Next St)
--appEvent st (skipEvent -> _) = drawThumbnail st >> M.continue st
--appEvent st (T.VtyEvent ev) = do
--  st' <- T.handleEventLensed st videoList L.handleListEvent ev
--  when (getSelectedVideoIndex st /= getSelectedVideoIndex st') $ drawThumbnail st'
--  processControls st' ev
--appEvent st _ = M.continue st
appEvent :: St -> T.BrickEvent WName CEvent -> T.EventM WName (T.Next St)
appEvent st =
  \case
    T.VtyEvent (V.EvResize _ _) -> redraw
    T.VtyEvent V.EvGainedFocus -> redraw
    T.AppEvent RefreshThumbnail -> redraw
    T.VtyEvent ev -> do
      st' <- T.handleEventLensed st videoList L.handleListEvent ev
      when (getSelectedVideoIndex st /= getSelectedVideoIndex st') $ drawThumbnail st'
      processControls st' ev
    _ -> M.continue st
  where
    redraw = drawThumbnail st >> M.continue st

listDrawElement :: Bool -> Video -> T.Widget WName
listDrawElement sel vid = widget
  where
    act :: Action = vid ^. action & fromMaybe NoAction
    widget = vidW & padLeftRight 1 & applyAttrs
    titleStr = padRight T.Max $ str $ vid ^. title
    detailsStr =
      padRight T.Max $ padLeft (T.Pad 2) $ str $ "[" <> vid ^. channelTitle <> "] " <> formatDateVarLen (vid ^. date)
    vidW = titleStr <=> detailsStr
    applyAttrs =
      case (act, sel) of
        (SemiHide, False)  -> withAttr semiHideAttr
        (SemiHide, True)   -> withAttr semiHideSelectedAttr
        (Highlight, False) -> withAttr highlightAttr
        (Highlight, True)  -> withAttr highlightSelectedAttr
        _                  -> Pre.id

customAttr :: A.AttrName
customAttr = L.listSelectedAttr <> "custom"

semiHideAttr :: A.AttrName
semiHideAttr = L.listAttr <> "SemiHide"

highlightAttr :: A.AttrName
highlightAttr = L.listAttr <> "Highlight"

semiHideSelectedAttr :: A.AttrName
semiHideSelectedAttr = L.listSelectedAttr <> "SemiHide"

highlightSelectedAttr :: A.AttrName
highlightSelectedAttr = L.listSelectedAttr <> "Highlight"

theMap :: A.AttrMap
theMap =
  A.attrMap
    V.defAttr
    [ (L.listAttr, V.brightWhite `BU.on` V.black)
    , (L.listSelectedAttr, V.brightWhite `BU.on` V.magenta `V.withStyle` V.bold)
    , (customAttr, BU.fg V.red)
    , (semiHideAttr, V.brightBlack `BU.on` V.black)
    , (semiHideSelectedAttr, V.white `BU.on` V.brightBlack)
    , (highlightAttr, V.brightYellow `BU.on` V.black `V.withStyle` V.bold)
    , (highlightSelectedAttr, V.brightYellow `BU.on` V.magenta)
    ]

theApp :: M.App St CEvent WName
theApp =
  M.App
    { M.appDraw = drawUI
    , M.appChooseCursor = M.showFirstCursor
    , M.appHandleEvent = appEvent
    , M.appStartEvent = return
    , M.appAttrMap = const theMap
    }

listTUI :: Config -> Result -> IO ()
listTUI cfg res = do
  eventChan <- BBch.newBChan 10
  let buildVty = V.mkVty V.defaultConfig
  initialVty <- buildVty
  BBch.writeBChan eventChan RefreshThumbnail
  let initialState = St {_stResult = res, _stVideoList = vidList, _stConfig = cfg, _stEventChannel = eventChan}
  void $ M.customMain initialVty buildVty (Just eventChan) theApp initialState
  where
    filteredVideos = (res ^. videos) & filter actionIsNotHide
    vidList = L.list VideoList (Vec.fromList filteredVideos) 2
    actionIsNotHide v = v ^. action /= Just Hide
