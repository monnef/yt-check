# yt-check

CLI tool for showing information about new videos on given YouTube channels.

# Interfaces
## List TUI
![](screenshots/list_tui_01.png)
Thumbnail support via [Terminology](https://www.enlightenment.org/about-terminology) and [timg](https://gitlab.com/monnef/timg).

## Terminal
![](screenshots/terminal_01.png)

# Installation

## Requirements

* [stack](http://haskellstack.org)
* *optionally* for ListTUI
  * [Terminology](https://www.enlightenment.org/about-terminology)
  * [timg](https://gitlab.com/monnef/timg)

## Install to stack bin directory

```sh
$ git clone git@gitlab.com:monnef/yt-check.git
$ cd yt-check
$ stack install
$ cp yt-check.sample.json yt-check.json # see configuration section
$ yt-check
```

# Configuration

The configuration file is named `yt-check.json` and is read from current directory.
You can base your configuration on [`yt-check.sample.json`](yt-check.sample.json).

Field types are based on Haskell types - e.g. `[]` signifies an array, `Maybe` is an optional field.

## `apiKey :: String`
A YouTube API key - [guide](https://developers.google.com/youtube/v3/getting-started)

## `channels :: [Channel]`
A list of channels for which video lists will be retrieved.

### type `Channel`

* `id :: String` - channel identifier, e.g. `UCd2xKD82vb-LbFEgE6MPsGw`
* `label :: String` - text shown in the UI

## `maxResults :: Int`
A number of videos to fetch per channel. The more videos and channels you have, the more pricey API-wise one program run will be.

## `reverse :: Maybe Bool`
Reverse order of results.

## `operations :: [Operation]`
A list of operations run on every video. Serves to control visibility of videos, how results will be presented.

### type `Operation`
* `regex :: String` - regular expression run against video title to determine whether `Action` will be applied or not
* `action :: Action` - an action attached to a video if `regex` passes

### type `Action`
One of strings:
* `Hide` - will not be shown at all
* `SemiHide` - use less contrast color and/or background
* `Highlight` - use more contrast color and/or background
* `NoAction` - render as usual (may be useful to turn off operation without having to delete it)

## `interface :: Interface`
One of strings:
* `TUIList` - terminal UI with a list widget on the left and a big thumbnail on the right (requires [Terminology](https://www.enlightenment.org/about-terminology) and [timg](https://gitlab.com/monnef/timg), or similar setup)
* `Terminal` - one line represents one video, uses colors
* `RawTerminal` - unformatted output, probably only useful for debugging purposes

## `previewCommand :: Maybe String`
A shell command which will be run every time when video thumbnail needs to be rendered.
It is a template using `$` to mark variables for replacement.

Example:
```sh
timg $path --size $width,$height --position $x,$y
```

### Variables
* `path` - URL to image file
* `x` - column (0-based)
* `y` - row (0-based)
* `width` - width of free space reserved for the image
* `height` - height of free space reserved for the image

## `controls :: Maybe [ControlDescription]`

Key bindings.

### type `ControlDescription`
* `event :: Event`
* `action :: ControlAction`

### type `Event`
Defined in [vty library](https://hackage.haskell.org/package/vty-5.25.1/docs/Graphics-Vty-Input.html#t:Event). Some examples:

* `EvKey KEsc []` - <kbd>ESC</kbd>
* `EvKey (KChar 'q') []` - <kbd>Q</kbd>
* `EvKey KEnter [MShift]` - <kbd>Shift</kbd> + <kbd>Enter</kbd>

### type `ControlAction`
One of:
* `RunCommandControlAction String`
* `QuitControlAction`

See [how Aeson represents sum types](#aeson-sum-types).

# Aeson - sum types
Aeson library is used for parsing JSON (e.g. a configuration file).

For example type `data X = RunCommandControlAction String` and its value `RunCommandControlAction "yes"` will be represented in JSON as:
```json
{
  "tag": "RunCommandControlAction",
  "contents": "yes"
}
```

You can find more examples in the sample configuration file.

# Notes
* [x] customizable controls
* [x] running custom commands
* [x] remove 1-based coordinates when timg supports 0-based ones
* [ ] caching?
* [ ] config file discovery (currently always pwd)
* [ ] command line argument to chose config file name?
* [ ] mouse support?
* [ ] themes?

# License
**GPLv3**
