{-# OPTIONS_GHC -F -pgmF htfpp #-}
{-# LANGUAGE OverloadedStrings #-}

module UtilsTest where

import           Data.Either
import           Data.Maybe
import           Test.Framework

import           SampleData
import           YTC.Data
import           YTC.Lib
import           YTC.Utils

test_combineResults :: IO ()
test_combineResults = do
  t [] (Right [])
  t [Left 0] (Left [0])
  t [Left 0, Left 1] (Left [0, 1])
  t [Left 0, Right "a"] (Left [0])
  t [Right "a", Right "b"] (Right ["a", "b"])
  where
    t :: [Either Int String] -> Either [Int] [String] -> IO ()
    t i e = assertEqual e (combineEithers i)
