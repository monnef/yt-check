{-# OPTIONS_GHC -F -pgmF htfpp #-}
{-# LANGUAGE FlexibleContexts #-}

module Main where

import           Test.Framework

import {-@ HTF_TESTS @-} ParseTest
import {-@ HTF_TESTS @-} UtilsTest

main :: IO ()
main = htfMain htf_importedTests
